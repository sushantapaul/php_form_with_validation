<?php include('./pertials/header.php'); ?>
<?php include('./pertials/navbar.php'); ?>

<?php
    session_start();
    include('connection.php');

    $id = $_GET['id'];

    $sql2 = "SELECT * FROM `students` ORDER BY id DESC LIMIT 1";
    $row2 = $conn->query($sql2);
    $data2 = $row2->fetch_assoc();

    if($id > $data2['id']) {
        $_SESSION["msz"] = "No Data Found";
        header('Location: ./index.php');
    } else {
        $sql = "SELECT * FROM `students` WHERE id = $id";
        $row = $conn->query($sql);
        $data = $row->fetch_assoc();
    ?>

    <section>
        <div class="container">
            <div class="col-md-4 col-md-offset-4">
            <h2>Edit Information</h2>
                <div class="btn-danger">
                    <p>
                        <?php
                            if(isset($_SESSION["msz"])) {
                                echo $_SESSION["msz"];
                                session_unset();
                            }
                        ?>
                    </p>
                </div>
                <form action="./update.php" method="POST">
                    <input type="text" class="hidden" name="id" value="<?php echo $data['id']; ?>">
                    <div class="form-group">
                        <label for="">Student Name</label>
                        <input type="text" name="name" class="form-control" value="<?php echo $data['name']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="">Class</label>
                        <input type="text" name="class" class="form-control" value="<?php echo $data['class']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="">Email address</label>
                        <input type="email" name="email" class="form-control" value="<?php echo $data['email']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="">Mobile</label>
                        <input type="tel" name="mobile" name="class" class="form-control" value="<?php echo $data['mobile']; ?>">
                    </div>
                    <button type="submit" name="update" class="btn btn-default">update</button>
                </form>
            </div>
        </div>
    </section>

    <?php } ?>

<?php include('./pertials/footer.php'); ?>