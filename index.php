<?php include('./pertials/header.php'); ?>
<?php include('./pertials/navbar.php'); ?>

<?php
    session_start();
    include('./connection.php');

    $sql = "SELECT * FROM `students`";
    $row = $conn->query($sql);
    $conn->close();
?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1>Student List</h1>
                <div class="btn-danger">
                    <p>
                        <?php
                            if(isset($_SESSION["msz"])) {
                                echo $_SESSION["msz"];
                                session_unset();
                            }
                        ?>
                    </p>
                </div>
                <table class="table table-bordered">
                    <thead>
                        <th>Id</th>
                        <th>Student Name</th>
                        <th>Class</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Action</th>
                    </thead>
                    <?php foreach($row as $value) { ?>
                    <tbody>
                        <td><?php echo $value['id']; ?></td>
                        <td><?php echo $value['name']; ?></td>
                        <td><?php echo $value['class']; ?></td>
                        <td><?php echo $value['email']; ?></td>
                        <td><?php echo $value['mobile']; ?></td>
                        <td>
                            <a class="btn btn-primary btn-xs" href="./view.php?id=<?php echo $value['id']; ?>">view</a>
                            <a class="btn btn-success btn-xs" href="./edit.php?id=<?php echo $value['id']; ?>">edit</a>
                            <a class="btn btn-danger btn-xs" href="./delete.php?id=<?php echo $value['id']; ?>">delete</a>
                        </td>
                    </tbody>
                    <?php }?>
                </table>
            </div>
        </div>
    </div>
</section>


<?php include('./pertials/footer.php'); ?>