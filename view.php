<?php include('./pertials/header.php'); ?>
<?php include('./pertials/navbar.php'); ?>

<?php
    session_start();
    include('connection.php');

    $id = $_GET['id'];

    $sql2 = "SELECT * FROM `students` ORDER BY id DESC LIMIT 1";
    $row2 = $conn->query($sql2);
    $data2 = $row2->fetch_assoc();

    if($id > $data2['id']) {
        $_SESSION["msz"] = "No Data Found";
        header('Location: ./index.php');
    } else {
        $sql = "SELECT * FROM `students` WHERE id = $id";
        $row = $conn->query($sql);
        $data = $row->fetch_assoc();
    
        $conn->close();
    ?>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h2>Students Profile:</h2>
                    <table class="table table-bordered">
                        <tr>
                            <td>Id</td>
                            <td><?php echo $data['id']; ?></td>
                        </tr>
                        <tr>
                            <td>Student Name</td>
                            <td><?php echo $data['name']; ?></td>
                        </tr>
                        <tr>
                            <td>Class</td>
                            <td><?php echo $data['class']; ?></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><?php echo $data['email']; ?></td>
                        </tr>
                        <tr>
                            <td>Mobile</td>
                            <td><?php echo $data['mobile']; ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <?php }  ?>

<?php include('./pertials/footer.php'); ?>